use std::thread;
use std::env;
extern crate rand;
use rand::distributions::{IndependentSample, Range};

fn monte_carlo_pi(num_runs : usize, num_threads : usize) -> f64 {
    let mut threads = vec![];
    let runs_per_thread = num_runs / num_threads;
    for _ in 0..num_threads {
        threads.push(thread::spawn(move || {
            let mut sum = 0;
            for _ in 0..runs_per_thread {

                let between = Range::new(0f64, 1.);
                let mut rng = rand::thread_rng();

                let a = between.ind_sample(&mut rng);
                let b = between.ind_sample(&mut rng);
                if a*a + b*b <= 1. {
                    sum += 1;
                }
            }
            sum
        }));
    }
    
    let mut sum = 0;
    for t in threads {
        let res = t.join();
        sum += res.unwrap()
    }
    let sum = sum;

    sum as f64 * 4.0 / num_runs as f64
}

fn main() {
    println!("Initializing...");

    let args : Vec<_> = env::args().collect();
    let num_runs = if args.len() > 1 {
        args[1].parse::<usize>().unwrap()
    }
    else {
        10_000_000
    };
    println!("Runs:\t\t{}", num_runs);

    let num_threads = if args.len() > 2 {
        args[2].parse::<usize>().unwrap()
    }
    else {
        3
    };
    println!("Threads:\t{}", num_threads);

    let pi = monte_carlo_pi(num_runs, num_threads);
    println!("Pi:\t\t{}", pi);
}
